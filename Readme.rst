===============
My Emacs Config
===============

:Author: Paresh Malalur
.. contents:: Table of Contents
.. section-numbering::


Installation
============

Start from a fresh setup (remove ``.emacs.d`` and ``.emacs`` files)

* Clone this repo to ``~/.emacs.d``::

    git clone git@bitbucket.org:pareshmg/myemacs.git ~/.emacs.d

* Set up the repo and submodules (checking out spacemacs)::

    git submodule update --init

* Create a ``~/.me.el`` with the contents::

    ;;; package --- personal settings
    ;;; Commentary:
    ;;; Code:
    (defvar me-my-email "<email>" "Users email.")
    (defvar me-my-name "<name>" "Users name.")
    (defvar me-my-email-text "<username> at <host> dot <suffix>" "Web safe email.")
    (provide '.me)
    ;;; .me.el ends here


* Open Emacs and wait for the basic setup to complete
