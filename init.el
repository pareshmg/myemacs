;;; init.el -- Main Emacs Initialization File

;;; Commentary:
;;; This file is loaded first by emacs if '~/.emacs' does not exist.

;;; Code:

;; Record the current value of 'user-emacs-directory' since we will be
;; overriding it later for spacemacs.

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
                                        ;(package-initialize)

(setq evil-want-keybinding nil)
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")

(defconst emacs-dir user-emacs-directory
  "Emacs configuration directory (pre-spacemacs).")

;; Tell spacemacs to find the configuration in the local spacemacs.d, which
;; keeps this repository self-contained (no need for ~/.spacemacs to be linked)
(setenv "SPACEMACSDIR" (concat emacs-dir "spacemacs.d/"))

;; Override user-emacs-directory so that spacemacs can load itself the way that
;; it wants.
(setq user-emacs-directory (concat emacs-dir "spacemacs/"))

;; Now load spacemacs.
(load (concat user-emacs-directory "init.el"))

;;; init.el ends here
(put 'set-goal-column 'disabled nil)
